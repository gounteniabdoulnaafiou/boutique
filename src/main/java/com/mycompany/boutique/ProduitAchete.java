/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.util.Objects;

/**
 *
 * @author GOUNTENI
 */
public class ProduitAchete {
    
    private int quantite = 1;
    private double remise = 0.0;
    private Produit achete;

    public ProduitAchete() {
    }

    public ProduitAchete(int quantite, double remise, Produit achete) {
        this.quantite = quantite;
        this.remise = remise;
        this.achete = achete;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.quantite;
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 53 * hash + Objects.hashCode(this.achete);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.achete, other.achete)) {
            return false;
        }
        return true;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public Produit getAchete() {
        return achete;
    }

    public void setAchete(Produit achete) {
        this.achete = achete;
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "quantite=" + quantite + ", remise=" + remise + ", achete=" + achete + '}';
    }
    //prix total sur le produit sans la remise
    public double getPrixTotal(){
        
        return quantite*(achete.getPrixUnitaire());      
                   
    }
    //calcul de la remise qui permettra de calculer le prix total 
    public double getRemiseProduit(){
        return quantite*achete.getPrixUnitaire()*remise;
    }
}
