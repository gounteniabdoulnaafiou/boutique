/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.io.Serializable;
import java.time.LocalDate;


/**
 *
 * @author GOUNTENI
 */
public class Produit implements Serializable {
    
    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Produit other = (Produit) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    

    public Produit() {
    }

    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", libelle=" + libelle + ", prixUnitaire=" + prixUnitaire + ", datePeremption=" + datePeremption + ", categorie=" + categorie + '}';
    }
    
    
    
    
    public boolean estPerimer(){
        
        return 
               LocalDate.now().getDayOfMonth() > this.datePeremption.getDayOfMonth() && 
               LocalDate.now().getMonthValue() > this.datePeremption.getMonthValue() &&
               LocalDate.now().getYear() > this.datePeremption.getYear();                
        
    }
    
    public boolean estPerimer(LocalDate ref){
        
        return 
                ref.getDayOfMonth() > this.datePeremption.getDayOfMonth() && 
                ref.getMonthValue() > this.datePeremption.getMonthValue() &&
                ref.getYear() > this.datePeremption.getYear();
    }
}
