/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author GOUNTENI
 */
public class Client extends  Personne{
    
    private String carteVisa;

    public Client() {
    }

    public Client(long id, String nom, String prenom, LocalDate dateNaissance, String carteVisa) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
    }

    public Client(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.carteVisa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.carteVisa, other.carteVisa)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Client{" + "carteVisa=" + carteVisa + '}';
    }
    
    
}
