/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author GOUNTENI
 */
public class Boutique {
    
    
    public static void main(String[] args) {
        
        //creation des produits
        Produit[] produit = new Produit[5];
        
        for(int i = 1; i<5 ;i++){
            produit[i] = new  Produit(i, "libelle"+i, 'i'+200, LocalDate.now());
        }
        
        //creation des categories
        
        ArrayList prodcat1 = new ArrayList();
        prodcat1.add(produit[1]);
        prodcat1.add(produit[2]);
        
        ArrayList prodcat2 = new ArrayList();
        prodcat2.add(produit[3]);
        prodcat2.add(produit[4]);

        Categorie cate1 = new Categorie(1, "categorie1", "description de la categorie1", prodcat1);
        Categorie cate2 = new Categorie(1, "categorie2", "description de la categorie2", prodcat2);
        
        //creation des produits achete
        
        ProduitAchete produitachete1 = new ProduitAchete(4, 0.05, produit[1]);
        ProduitAchete produitachete2 = new ProduitAchete(8, 0.25, produit[2]);
        ProduitAchete produitachete3 = new ProduitAchete(2, 0.025, produit[4]);
        
        //creation d'un employe
        Employe employe = new Employe(1, "GOUNTENI", "Abdoul Naafiou",  LocalDate.of(2000,01,23), "aprf-74-sd", LocalDate.of(2012,8,15));

        
        //creation des achats
        ArrayList produitsachetes = new ArrayList();
        produitsachetes.add(produitachete1);
        produitsachetes.add(produitachete2);
        produitsachetes.add(produitachete3);
        
        Achat achat = new Achat(1, LocalDate.now(), 0.002, produitsachetes, employe);
        
       
        System.out.println("description d'un produit : " +produit[1].toString());
        System.out.println("description de la catégorie : " +cate1.toString());
        System.out.println("le prix du produit achete  " +produitachete1.getAchete().getId()+ "  est  : " +produitachete1.getPrixTotal() );
        System.out.println("la description de l'achat "+achat.getId() + " : " +achat.toString());
        System.out.println("la remise total de l'achat "+achat.getId() +" est " +achat.getRemiseTotal());
        System.out.println("le prix total de l'achat "+achat.getId() +" est " +achat.getPrixTotal());
    }
}
