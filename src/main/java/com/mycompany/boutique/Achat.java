/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author GOUNTENI
 */
public class Achat implements Serializable{
    private long id;
    private double remise = 0.0;
    private LocalDate dateAchat;
    private List<ProduitAchete> produitachete;
    private Employe employe;

    public Achat() {
    }

    public Achat(long id, LocalDate dateAchat, double remise,  List<ProduitAchete> produitachete, Employe employe) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.produitachete = produitachete;
        this.employe = employe;
        this.remise = remise;
    }



    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public List<ProduitAchete> getProduitachete() {
        return produitachete;
    }

    public void setProduitachete(List<ProduitAchete> produitachete) {
        this.produitachete = produitachete;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", produitachete=" + produitachete + ", employe=" + employe + '}';
    }

    public double getRemiseTotal(){
        double r = 0.0;
        for (ProduitAchete produitAchete : produitachete) {
            r += produitAchete.getRemiseProduit();
        }
        
        return r + r*remise;
    }
    
    public double getPrixTotal(){
        double p = 0.0;
        for (ProduitAchete produitAchete : produitachete) {
            p += produitAchete.getPrixTotal();
        }
        return p - getRemiseTotal();
    }
}
