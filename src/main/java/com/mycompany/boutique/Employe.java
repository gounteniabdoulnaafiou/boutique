/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author GOUNTENI
 */
public class Employe extends Personne{
    private String cnss;
    private LocalDate dateEmbauche;
    private List<Achat> achats;

    public Employe(long id, String nom, String prenom, LocalDate dateNaissance, String cnss, LocalDate dateEmbauche, List<Achat> achats) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
        this.achats = achats;
    }

    public Employe(String cnss, LocalDate dateEmbauche, List<Achat> achats) {
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
        this.achats = achats;
    }

    public Employe(long id, String nom, String prenom, LocalDate dateNaissance,String cnss, LocalDate dateEmbauche) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    public Employe(){
        
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.cnss);
        hash = 67 * hash + Objects.hashCode(this.dateEmbauche);
        hash = 67 * hash + Objects.hashCode(this.achats);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {
            return false;
        }
        if (!Objects.equals(this.dateEmbauche, other.dateEmbauche)) {
            return false;
        }
        if (!Objects.equals(this.achats, other.achats)) {
            return false;
        }
        return true;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public List<Achat> getAchats() {
        return achats;
    }

    public void setAchats(List<Achat> achats) {
        this.achats = achats;
    }

    @Override
    public String toString() {
        return "Employe{" + "cnss=" + cnss + ", dateEmbauche=" + dateEmbauche + ", achats=" + achats + '}';
    }
    
    
    
}
